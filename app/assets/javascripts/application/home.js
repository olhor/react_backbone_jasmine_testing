$(document).ready(() => {
    if($('#home').length > 0) {
        let model = new App.Models.Post()
        ReactDOM.render(
            React.createElement(App.Components.Post, {model: model}, null),
            $('#home')[0]
        )
    }
})