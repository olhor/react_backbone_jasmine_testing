//= require_self
//= require_tree ./templates
//= require_tree ./models
//= require_tree ./views
//= require_tree ./routers
//= require_tree ./components

window.App = {
    Models: {},
    Collections: {},
    Routers: {},
    Views: {},
    Components: {}
}