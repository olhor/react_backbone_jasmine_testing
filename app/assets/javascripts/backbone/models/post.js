App.Models.Post = class extends Backbone.Model {
    constructor(attrs, opts) {
        super(attrs, opts)
        console.log('creating Post model')
    }

    defaults() {
        return {
            counter: 0,
            title: `Hello World`
        }
    }

    incrementCounter(){
        this.set('counter', this.get('counter') + 1)
    }

}

App.Collections.PostsCollection = class extends Backbone.Collection {
  model() {
      return App.Models.Post
  }
}