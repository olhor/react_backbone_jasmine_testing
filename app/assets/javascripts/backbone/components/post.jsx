App.Components.Post = class extends React.Component {
    constructor(props) {
        super(props);
        console.log('creating Post component')
        this._handleChange = this._handleChange.bind(this);
    }

    componentDidMount() {
        this.props.model.on('change', this._handleChange);
    }

    componentWillUnmount() {
        this.props.model.off('change', this._handleChange);
    }

    render() {
        return <h1 onClick={this._onClick.bind(this)}>{`${this.props.model.get('title')}, ${this.props.model.get('counter')}`}</h1>;
    }

    _handleChange() {
        this.forceUpdate();
    }

    _onClick() {
        this.props.model.incrementCounter()
    }
}