describe("App.Components.Post", () => {
    let post, model, elem

    beforeEach(function() {
        model = new App.Models.Post
        post = React.createElement(App.Components.Post, {model: model}, null)
        elem = ReactTestUtils.renderIntoDocument(post)
    });

    it("should be created with 'Post' model", () => {
        expect(post.props.model).toEqual(model)
    })

    it("should change text after click", () => {
        let node = ReactDOM.findDOMNode(elem)
        expect(node.textContent).toEqual('Hello World, 0')
        ReactTestUtils.Simulate.click(node);
        expect(node.textContent).toEqual('Hello World, 1')
    })
})