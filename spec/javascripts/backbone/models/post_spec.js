describe("App.Models.Post", () => {
    let post

    beforeEach(function() {
        post = new App.Models.Post()
    });

    it("should have defaults", () => {
        expect(post.attributes).toEqual({title: 'Hello World', counter: 0})
    })

    it("should increment counter", () => {
        post.incrementCounter()
        expect(post.get('counter')).toEqual(1)
    })
})